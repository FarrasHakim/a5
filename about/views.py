from .models import Comment
from django.http import HttpResponse, JsonResponse
from django.shortcuts import render

# Create your views here.


def makeComment(request):
    my_dict = {
        "status": "Fail"
    }
    if request.method == "POST":
        username = request.POST["username"]
        comment = request.POST["comment"]
        obj = Comment.objects.create(username=username, comment=comment)
        my_dict["status"] = "OK"
        my_dict["username"] = username
        my_dict["comment"] = comment
        added = "{dt:%b}. {dt.day}, {dt.year}, {dt:%r}".format(dt=obj.added)
        added = added[:added.rfind(':')] + ' '
        added += 'a.m.' if obj.added.strftime("%p") == 'AM' else 'p.m.'
        my_dict["added"] = added
    return JsonResponse(my_dict)


def profile(request):
    context = {}
    if Comment.objects.all().count() > 0:
        context['comments'] = Comment.objects.all().order_by('-added')
    return render(request, "profile.html", context)
