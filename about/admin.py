from django.contrib import admin
from .models import Comment


class CommentAdmin(admin.ModelAdmin):
    list_display = ["username", "comment", "added"]
    list_filter = ["added"]
    search_fields = ["username", "comment"]

    class Meta:
        model = Comment


admin.site.register(Comment, CommentAdmin)
