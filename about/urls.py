from django.urls import path
from about import views

urlpatterns = [
    path('', views.profile),
    path('comment/', views.makeComment),
]
