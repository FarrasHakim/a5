from .models import Comment
from .views import *
from django.test import TestCase, Client
from django.urls import resolve
import json


class ProfilePageUnitTest(TestCase):
    def test_url_about_us_exist(self):
        response = Client().get('/profile/')
        self.assertEqual(response.status_code, 200)

    def test_template_used_by_profile(self):
        response = Client().get('/profile/')
        self.assertTemplateUsed(response, 'profile.html')

    def test_profile_func(self):
        found = resolve('/profile/')
        self.assertEqual(found.func, profile)

    def test_object_added(self):
        berita = Comment.objects.create(username="testTest", comment="Test Comment")
        counted = Comment.objects.all().count()
        self.assertEqual(counted, 1)

    def test_add_object_and_render(self):
        username = "Test"
        comment = "Ini hanya test saja"
        komentar = Comment.objects.create(username=username, comment=comment)
        response = Client().get("/profile/")
        html_response = response.content.decode('utf8')
        self.assertIn(username, html_response)
        self.assertIn(comment, html_response)

    def test_url_comment_maker_exist(self):
        response = Client().get('/profile/comment/')
        self.assertEqual(response.status_code, 200)

    def test_comment_maker_func(self):
        found = resolve('/profile/comment/')
        self.assertEqual(found.func, makeComment)

    def test_post_comment(self):
        request = {
            'username': "Test",
            'comment': "Test Doang Kok Gan",
        }
        response = Client().post('/profile/comment/', request)
        json_response = response.json()
        self.assertEqual(json_response["comment"], request["comment"])
        self.assertEqual(json_response["username"], request["username"])
        self.assertEqual(response.status_code, 200)
