from django.db import models

# Create your models here.


class Comment(models.Model):
    username = models.CharField(max_length=50)
    comment = models.TextField()
    added = models.DateTimeField(auto_now=True, auto_now_add=False)
