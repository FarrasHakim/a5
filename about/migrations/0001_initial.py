# Generated by Django 2.1.1 on 2018-12-05 05:17

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Comment',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('username', models.CharField(max_length=50)),
                ('comment', models.TextField()),
                ('added', models.DateTimeField(auto_now=True)),
            ],
        ),
    ]
