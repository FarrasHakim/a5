from django.urls import path
from .views import *

urlpatterns = [
    path('login/', logins, name='login'),
    path('logout/', logouts, name='logout'),
    path('homez/', homez, name='homez'),
]