from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from urllib.request import urlopen
from django.contrib.auth import logout as auth_logout
from django.contrib import messages
import requests

def logins(request):
    response = {}
    return render(request, 'login.html', response)

def logouts(request):
    request.session.flush()
    auth_logout(request)
    response = {}
    return HttpResponseRedirect('/login/')

def homez(request):
    return HttpResponseRedirect('/program_donasi/')