from django.test import TestCase
from django.apps import apps
from django.http import HttpRequest
from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from django.contrib.auth import get_user_model

# Create your tests here.
class Story9Test(TestCase):
    def setUp(self):
        User = get_user_model()
        user = User.objects.create_user('lol', 'lol@gmail.com', 'lol')
    
    def test_story9_using_landing_template(self):
        client = Client()
        client.login(username="lol", password="lol")
        response = client.get('/program_donasi/')
        self.assertIn("Hello, lol", response.content.decode("utf8"))

    def test_loginpage_is_exist(self):
        response = Client().get('/login/') 
        self.assertEqual(response.status_code,200)

    def test_login(self):
        client = Client()
        client.login(username="lol", password="lol")
        response = client.get('/logout/')
        self.assertEquals(len(dict(client.session)), 0)
        self.assertEquals(response.status_code,302)
    
    def test_daftar_using_daftar_template(self):
        response = Client().get('/homez/')
        self.assertEquals(response.status_code,302)