from django.contrib import admin
from .models import Berita


class BeritaAdmin(admin.ModelAdmin):
    list_display = ["title", "updated", "added"]
    list_filter = ["updated", "added"]
    search_fields = ["title", "content"]

    class Meta:
        model = Berita


admin.site.register(Berita, BeritaAdmin)
# Register your models here.
