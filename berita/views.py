from django.shortcuts import render, get_object_or_404
from .models import Berita
from django.core.paginator import EmptyPage, PageNotAnInteger, Paginator
from django.contrib import messages
from django.http import HttpResponseRedirect


def berita_detail(request, id=None):
    instance = get_object_or_404(Berita, id=id)
    context = {
        'instance': instance}
    return render(request, 'berita_detail.html', context)


def berita_default(request):
    queryset_list = Berita.objects.all()
    if queryset_list.count() == 0:
        return render(request, 'berita.html')
    Latest_3 = []
    if queryset_list.count() > 3:
        for x in range(0, 3):
            Latest_3.append(queryset_list[x])
    else:
        for x in range(0, queryset_list.count()):
            Latest_3.append(queryset_list[x])
    paginator = Paginator(queryset_list, 8)
    page_var = 'page'
    page = request.GET.get(page_var)
    query = paginator.get_page(page)
    context = {
        'query': query,
        'list': Latest_3
    }
    return render(request, 'berita.html', context)
