from django.test import TestCase, Client
from django.urls import resolve
from .views import *
from .models import Berita


class BeritaUnitTest(TestCase):
    def test_url_berita_exist(self):
        response = Client().get('/berita/')
        self.assertEqual(response.status_code, 200)

    def test_template_used_by_berita(self):
        response = Client().get('/berita/')
        self.assertTemplateUsed(response, 'berita.html')

    def test_object_added(self):
        berita = Berita.objects.create(title="Test", content="Ini hanya test saja", image_url="https://cdn-images-1.medium.com/max/2000/1*C51FyB82wHdzRTgEIsYKPw.jpeg")
        counted = Berita.objects.all().count()
        self.assertEqual(counted, 1)

    def test_add_object_and_render(self):
        title = "Test"
        content = "Ini hanya test saja"
        image_url = "https://cdn-images-1.medium.com/max/2000/1*C51FyB82wHdzRTgEIsYKPw.jpeg"
        berita = Berita.objects.create(title=title, content=content, image_url=image_url)
        berita_id = berita.id
        response = Client().get("/berita/detail/" + str(berita_id) + '/')
        html_response = response.content.decode('utf8')
        self.assertIn(title, html_response)
        self.assertIn(content, html_response)

    def test_berita_default_func(self):
        found = resolve('/berita/')
        self.assertEqual(found.func, berita_default)

    def test_berita_kasus_if_object_lessthan_3_default_func(self):
        berita1 = Berita.objects.create(title="Test Func 1", content="Ini test fungsi view", image_url="https://cdn-images-1.medium.com/max/2000/1*C51FyB82wHdzRTgEIsYKPw.jpeg")
        berita2 = Berita.objects.create(title="Test func 2", content="Ini test fungsi view juga", image_url="https://cdn-images-1.medium.com/max/2000/1*C51FyB82wHdzRTgEIsYKPw.jpeg")
        response = Client().get('/berita/')
        html_response = response.content.decode('utf8')
        self.assertIn(berita1.title, html_response)
        self.assertIn(berita2.title, html_response)

    def test_berita_kasus_if_object_morethan_3_default_func(self):
        berita1 = Berita.objects.create(title="Test Func 1", content="Ini test fungsi view", image_url="https://cdn-images-1.medium.com/max/2000/1*C51FyB82wHdzRTgEIsYKPw.jpeg")
        berita2 = Berita.objects.create(title="Test func 2", content="Ini test fungsi view juga", image_url="https://cdn-images-1.medium.com/max/2000/1*C51FyB82wHdzRTgEIsYKPw.jpeg")
        berita3 = Berita.objects.create(title="Test func 3", content="Ini pun test fungsi view pula", image_url="https://cdn-images-1.medium.com/max/2000/1*C51FyB82wHdzRTgEIsYKPw.jpeg")
        berita4 = Berita.objects.create(title="Test func 4", content="Ini test terakhir", image_url="https://cdn-images-1.medium.com/max/2000/1*C51FyB82wHdzRTgEIsYKPw.jpeg")

        response = Client().get('/berita/')
        html_response = response.content.decode('utf8')
        self.assertIn(berita1.title, html_response)
        self.assertIn(berita2.title, html_response)
        self.assertIn(berita3.title, html_response)
        self.assertIn(berita4.title, html_response)

    def test_berita_detail_url_exist_and_template(self):
        berita = Berita.objects.create(title="URL", content="Test Url exist gan", image_url="https://cdn-images-1.medium.com/max/2000/1*C51FyB82wHdzRTgEIsYKPw.jpeg")

        response = Client().get('/berita/detail/' + str(berita.id) + '/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'berita_detail.html')

    def test_get_absolute_url_and_str_in_models(self):
        berita = Berita.objects.create(title="Dynamic Url", content="Test dynamic urls from models", image_url="https://cdn-images-1.medium.com/max/2000/1*C51FyB82wHdzRTgEIsYKPw.jpeg")
        self.assertEqual(berita.__str__(), berita.title)
        self.assertEqual("/berita/detail/1/", berita.get_absolute_url())
