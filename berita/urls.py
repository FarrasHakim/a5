from django.urls import path
from berita import views

urlpatterns = [
    path('', views.berita_default,name="home"),
    path('detail/<id>/', views.berita_detail, name="news_detail"),
]
