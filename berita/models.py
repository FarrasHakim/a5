from django.db import models
from django.urls import reverse


class Berita(models.Model):
    title = models.CharField(max_length=120)
    content = models.TextField()
    added = models.DateTimeField(auto_now=False, auto_now_add=True)
    updated = models.DateTimeField(auto_now=True, auto_now_add=False)
    image_url = models.CharField(max_length=120, null=True, blank=True)

    def __str__(self):
        return self.title

    def get_absolute_url(self):
        return reverse('news_detail', kwargs={"id": self.id})

    class Meta:
        ordering = ["-added", "-id"]
# Create your models here.
