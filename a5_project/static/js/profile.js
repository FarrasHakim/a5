var func = function() {
    $('#alert').removeClass("alert alert-danger");
    $('#alert').html("");
}

$(function(){
    $('#commentForm').on('submit', function(e) {
        e.preventDefault();
        if ($('#commentArea').val() != "") {
            $.ajax({
                url : '/profile/comment/',
                type : "POST",
                data : {
                    username : $('#username').val(),
                    comment : $('#commentArea').val(),
                    csrfmiddlewaretoken : $("input[name=csrfmiddlewaretoken]").val(),
                },
                success : function (response) {
                    var text = '<div class="row"><div class="col-md-4"><img src="http://www.eurogeosurveys.org/wp-content/uploads/2014/02/default_profile_pic.jpg" height ="30px"alt="">' +
                                response.username +  '<br></div><div class="col-md-6"><small>On '+ response.added + '</small><br>"' + response.comment + '"</div></div><br><br><br>'
                    $('#commentArea').val("");
                    if ($("#comments").html().replace( /\s/g, '') == "NoCommentsAvailable") {
                        $("#comments").html(text);
                    } else {
                        $("#comments").prepend(text);
                    }
                }
            });
        } else {
            $('#alert').addClass("alert alert-danger");
            $('#alert').html("Tolong isi kolom komentar");
        }
    });
    $('#commentArea').keyup(func);
})
