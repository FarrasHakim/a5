"""a5_project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/2.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.urls import include, path
from django.conf import settings
from django.conf.urls import url, include
from django.conf.urls.static import static
from django.contrib import admin
from django.views.generic import RedirectView

urlpatterns = [
    path('', RedirectView.as_view(url='/profile')),
    url("", include("donasiDanForm.urls")),
    url(r'^admin/', admin.site.urls),
    url("", include("ppwbisa.urls")),
    url("", include("register.urls")),
    url("", include("logins.urls")),
    url(r'^auth/', include('social_django.urls', namespace='social')),
    path('berita/', include("berita.urls")),
    path('profile/', include("about.urls")),
    path('profile/', include("donasiDanForm.urls")),


] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
