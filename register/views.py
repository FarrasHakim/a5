from django.shortcuts import render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.contrib import messages
from .forms import Registration_Form
from .models import User_Data
from datetime import datetime, timedelta, date
import datetime
# Create your views here.

def daftar(request):
    response = {}
    liststatus = User_Data.objects.all()
    response['liststatus'] = liststatus
    response['form'] = Registration_Form
    
    if (request.method == 'POST'):
        # Create a form instance and populate it with data from the request (binding):
        form_jadwal = Registration_Form(request.POST)
        try:
            age_str = datetime.datetime.strptime(request.POST["tanggal_lahir"], "%Y-%m-%d")
        except:
            messages.error(request, "pesan error")
            return HttpResponseRedirect(reverse('daftar'))

        age = age_str.year
        date = datetime.datetime.now().year
        if ((date-age)<14):
            messages.error(request, "umur kurang")
            return HttpResponseRedirect(reverse('daftar'))
        # Check if the form is valid:
        if form_jadwal.is_valid():
            # process the data in form.cleaned_data as required (here we just write it to the model due_back field)
            form_jadwal.save()
			# redirect to a new URL
            return HttpResponseRedirect('/berita')
        else:
            errors = []
            for x in form_jadwal.errors:
                errors += form_jadwal.errors[x]
            messages.error(request, errors[-1])
            return HttpResponseRedirect(reverse('daftar'))
    
    else:
        return render(request, 'daftar.html', response)