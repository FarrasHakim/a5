from django import forms
from .models import User_Data

class Registration_Form(forms.ModelForm):
    email_error = {
        "required": "Fill the email address.",
        "invalid": "Email tidak valid"
    }

    tanggal_lahir= forms.DateField(required=True,widget=forms.DateInput(attrs={'type':'date','class':'form-control', "placeholder":"Tanggal lahir (format: MM/DD/YYYY"}))
    password = forms.CharField(required=True,widget=forms.PasswordInput(attrs={'class':'form-control',"placeholder":"Password"}))
    nama_lengkap = forms.CharField(required=True,help_text='100 characters max.',widget=forms.TextInput(attrs={'class':'form-control',"placeholder":"Nama Lengkap"}))
    email = forms.EmailField(required = True,help_text='A valid email address, please.',widget=forms.TextInput(attrs={'class':'form-control',"placeholder":"Email"}),error_messages=email_error)
    class Meta:
        model = User_Data
        fields = ('nama_lengkap','tanggal_lahir','email','password',)