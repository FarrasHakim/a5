from django.test import TestCase
from django.test import TestCase
from django.apps import apps
from django.http import HttpRequest

from django.test import TestCase, Client
from django.urls import resolve
from .views import daftar
from .models import User_Data
from .forms import Registration_Form

class RegisterPageTest(TestCase):
	def test_daftar_is_exist(self):
		response = Client().get('/daftar/') 
		self.assertEqual(response.status_code,200)

	def test_daftar_using_daftar_func(self):
		found = resolve('/daftar/')
		self.assertEqual(found.func, daftar)

	def test_daftar_using_daftar_template(self):
		response = Client().get('/daftar/')
		self.assertTemplateUsed(response, 'daftar.html')
	
	def test_daftar_success(self):
		response = Client().post('/daftar/', {"nama_lengkap":"niidz", "email":"annidasa@gmail.com", "tanggal_lahir":"2000-01-01", "password":"heheehe"}, follow=True)
		self.assertEqual(response.status_code, 200)
		self.assertEqual(User_Data.objects.all().count(), 1)
		
		html_response = response.content.decode('utf8')

	def test_daftar_not_complete(self):
	 	test = "a"*1000
	 	response = Client().post('/daftar/', {"nama_lengkap": test, "email": "", "tanggal_lahir": "", "password": ""}, follow=True)
	 	self.assertEqual(response.status_code, 200)
	 	self.assertEqual(User_Data.objects.all().count(), 0)
	 	html_response = response.content.decode('utf8')
	 	self.assertNotIn(test, html_response)

	def test_email_tidak_valid(self):
		response = Client().post('/daftar/', {"nama_lengkap":"niidz", "email":"annidasa@", "tanggal_lahir":"2000-01-01", "password":"heheehe"}, follow=True)
		self.assertEqual(response.status_code, 200)
		self.assertEqual(User_Data.objects.all().count(), 0)

		response = Client().post('/daftar/', {"nama_lengkap":"niidz", "email":"annidasa@gmail", "tanggal_lahir":"2000-01-01", "password":"heheehe"}, follow=True)
		self.assertEqual(response.status_code, 200)
		self.assertEqual(User_Data.objects.all().count(), 0)
		
		
	def test_pernah_regist(self):
		response = Client().post('/daftar/', {"nama_lengkap":"niidz", "email":"annidasa@gmail.com", "tanggal_lahir":"2000-01-01", "password":"heheehe"}, follow=True)
		self.assertEqual(response.status_code, 200)
		self.assertEqual(User_Data.objects.all().count(), 1)
		html_response = response.content.decode('utf8')
		response = Client().post('/daftar/', {"nama_lengkap":"niidz", "email":"annidasa@gmail.com", "tanggal_lahir":"2000-01-01", "password":"heheehe"}, follow=True)
		self.assertEqual(response.status_code, 200)
		self.assertEqual(User_Data.objects.all().count(), 1)

	def test_umur_kurang(self):
		response = Client().post('/daftar/', {"nama_lengkap":"niidz", "email":"annidasa@gmail.com", "tanggal_lahir":"2018-01-01", "password":"heheehe"}, follow=True)
		self.assertEqual(response.status_code, 200)
		self.assertEqual(User_Data.objects.all().count(), 0)
