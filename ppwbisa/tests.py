from django.test import TestCase, Client
from django.urls import resolve

from .views import index
from .models import list_program

# Create your tests here.
class TP1_ppw(TestCase):

    def test_check_url_programDonasi_exist(self):
        response = Client().get("/program_donasi/")
        self.assertEqual(response.status_code, 200)

    # def test_check_hello_string(self):
    #     response = self.client.get("/")
    #     html_response = response.content.decode("utf8")
    #     self.assertIn("Hello Dunia !", html_response)

    def test_template_used_by_programDonas(self):
        response = self.client.get("/program_donasi/")
        self.assertTemplateUsed(response, "index.html")

    # def test_add_object(self):
    #     program = list_program.objects.create(title="Nekat",detail="Wiiiiii")
    #     counted = list_program.objects.all().count()
    #     self.assertEqual(counted,1)
    def test_apalah(self):
        prog = list_program.objects.create(title="Test", detail="cuman test")
        count = list_program.objects.all().count()
        self.assertEqual(count,1)

        response = Client().get("/program_donasi/")
        self.assertIn("Test", response.content.decode("utf-8"))


	# def test_cek_karakter(self):
 #        cek = "a" * 100
 #        response = Client().get("/program_donasi", {"cek" : cek})
 #        self.assertEqual(program.objects.all().count(), 0)