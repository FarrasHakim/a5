from django.conf.urls import url
from .views import index

urlpatterns = [
    url(r"^program_donasi/$", index, name="index"),
]