from django.db import models

# Create your models here.
class list_program(models.Model):
    title = models.CharField(max_length=100)
    detail = models.TextField(max_length=400)
    amount = models.IntegerField(default=0)
    slug = models.SlugField()


    # def __str__(self):
    #     return self.title
