from django.conf.urls import url
from .views import programDonasi
from .views import profilePage

urlpatterns = [
    url(r"^program_donasi/(?P<slug>[\w-]+)/$", programDonasi, name="program donasi"),
    url(r"^profil/(?P<slug>.*)/$", profilePage, name="Profile Page"),
]