from django.shortcuts import render, redirect
from django.http import HttpResponse
from .forms import DonationForm
from .models import DonationMade
from ppwbisa.models import list_program
from django.contrib.auth.models import User
from django.contrib import messages

def programDonasi(request, slug):
    program = list(list_program.objects.filter(slug = slug))
    donatur = DonationMade.objects.all().values().filter(programName = program[0].title)
    context = {"donatur" : donatur}
    form = DonationForm(request.POST)

    if request.method =="POST" and form.is_valid(): 
        if program[0].title == form.cleaned_data['programName']:
            userEmail = form.cleaned_data['donorEmail']
            if User.objects.filter(email = userEmail).exists():
                donationAmount = form.cleaned_data['donationAmount']
                program[0].amount += donationAmount
                program[0].save()
                form.save()
                return redirect("/")
            else:
                context.update({
                    "form" : form,
                    "messages" : {"tags" : "warning", "message": "Alamat email belum terdaftar"}
                }) 
                return render(request, "DonationPageAndForm.html", context)
        else:
            context.update({
                "form" : form,
                "messages" : {"tags" : "warning", "message": "cek nama program yang ingin anda berikan donasi"}
            })  
            return render(request, "DonationPageAndForm.html", context)
    else:
        form = DonationForm(initial={"programName":program[0].title})
        context.update( {
            "form" : form
        }) 
        return render(request, "DonationPageAndForm.html", context)

def profilePage(request, slug):
    if request.user.is_authenticated:
        user = User.objects.get(username = slug)
        donasi = DonationMade.objects.all().values().filter(donorEmail = user.email)
        context = {"donasi":donasi}
        context["nama"] = user.first_name + " " +  user.last_name

        return render(request, "ProfilePage.html", context)
    else:
        return HttpResponse("<h1>Anda belum login</h1><br><a href='/login/'>Silahkan login terlebih dahulu</a>")