from django.contrib import admin
from .models import DonationMade

admin.site.register(DonationMade)

# Register your models here.
