from django.test import TestCase, Client
from django.urls import resolve
from django.http import HttpRequest
from .views import *
from .models import DonationMade
from ppwbisa.models import list_program
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from datetime import timedelta, date
from .forms import DonationForm
import unittest

from django.conf import settings
from django.test.client import Client

class donationAndFormTest(TestCase):
    def setUp(self):
        year = timedelta(days=365)
        list_program.objects.create(title="test program 1", detail="cuman test", amount=0, slug="test-program-1")
        User.objects.create_user(username="alice.bing", first_name="Alice", last_name="Bing", email="alice@company.com", password="testing123")

    def test_donation_page_exist(self):
        response = Client().get('/program_donasi/test-program-1/')
        self.assertEqual(response.status_code,200)

    def test_donation_page_using_daftar_func(self):
        found = resolve('/program_donasi/test-program-1/')
        self.assertEqual(found.func, programDonasi)

    def test_donation_page__using_donation_template(self):
        response = Client().get('/program_donasi/test-program-1/')
        self.assertTemplateUsed(response, 'DonationPageAndForm.html')
    
    def test_donate_with_valid_info(self):
        post = {"donorName": "Alice", 
        "donorEmail": "alice@company.com",
        "programName": "test program 1",
        "donationAmount": "50000"
        }
        response = Client().post('/program_donasi/test-program-1/', post, follow=True)
        self.assertEqual(response.status_code, 200)
        self.assertEqual(DonationMade.objects.all().count(), 1)
    
    def test_donate_with_invalid_email_address(self):
        post = {"donorName": "Alice", 
        "donorEmail": "Bob@company.com",
        "programName": "test program 1",
        "donationAmount": "50000"
        }
        response = Client().post('/program_donasi/test-program-1/', post, follow=True)
        self.assertIn("Alamat email belum terdaftar",response.content.decode('utf8'))
        self.assertEqual(DonationMade.objects.all().count(), 0)
    
    def test_donate_with_invalid_program_name(self):
        post = {"donorName": "Alice", 
        "donorEmail": "alice@company.com",
        "programName": "test program 2",
        "donationAmount": "50000"
        }
        response = Client().post('/program_donasi/test-program-1/', post, follow=True)
        self.assertIn("cek nama program yang ingin anda berikan donasi",response.content.decode('utf8'))
        self.assertEqual(DonationMade.objects.all().count(), 0)

    def test_user_authentication(self):
        c = Client()
        c.login(username='alice.bing', password='testing123')
        response = c.get('/profil/alice.bing/', follow=True)
        # print(response.content.decode('utf8'))
        self.assertIn("Hello, alice.bing", response.content.decode('utf8'))
        c.logout()
        response = c.get('/profil/alice.bing/', follow=True)
        self.assertIn("<h1>Anda belum login</h1><br><a href='/login/'>Silahkan login terlebih dahulu</a>", response.content.decode('utf8'))

        
