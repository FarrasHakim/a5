from django.db import models

class DonationMade(models.Model):
    donorName = models.CharField(max_length=100,default="Anonim")
    donorEmail = models.EmailField()
    programName = models.CharField(max_length=100)
    donationAmount = models.IntegerField()
    donationDateTime = models.DateTimeField(auto_now_add=True)
    asAnonymous = models.BooleanField(default=False)