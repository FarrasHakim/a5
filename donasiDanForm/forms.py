from django import forms
from .models import DonationMade

class DonationForm(forms.ModelForm):
    class Meta:
        model = DonationMade
        fields = ['donorName','donorEmail','programName','donationAmount','asAnonymous']
        widgets = {
            'donorName': forms.TextInput(attrs={'class' : 'form-control'}),
            'donorEmail': forms.TextInput(attrs={'class' : 'form-control'}),
            'programName': forms.TextInput(attrs={'class' : 'form-control'}),
            'donationAmount': forms.NumberInput(attrs={'class' : 'form-control'}),
            'asAnonymous': forms.CheckboxInput(attrs={'value':"Sebagai Anonim", 'class' : 'form-control'}),
        }
        labels = {
            "donorName": "Nama",
            'donorEmail': "Email",
            "programName": "Program",
            "donationAmount" : "Jumlah",
            "asAnonymous" : "Sebagai Anonim",
        }